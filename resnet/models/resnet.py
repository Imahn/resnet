import torch.nn as nn


class ResNet(nn.Module):
    def __init__(
        self,
        num_layers_per_block=[5, 5, 5],
        in_channels=3,
        out_channels=16,
        stride=(2, 2),
        padding_mode="zeros",
        num_classes=10,
        bottleneck=False,
        expansion=1,
    ):
        """Initialize ResNet class.

        Parameters:
            num_layers_per_block (List[int]): Number of layers per block. The 
                number of blocks is given by the length of the list.
            in_channels (int): Number of input channels for the input images.
            out_channels (int): Number of output channels for the first Conv2d
                layer.
            stride ((int, int)): Stride to be used in all Conv2d layers that
                downsample.
            padding_mode (str): Which padding mode to use at the borders of
                the images. Options: "zeros", "reflect", "replicate",
                "circular".
            num_classes (int): Number of classes of the CIFAR10 dataset.
            bottleneck (bool): Whether to use bottleneck architecture or not.
            expansion (int): In bottleneck architecture, first 1x1 conv layer
                reduces number of channels by this factor (except in the first 
                layer of each block), then 3x3 conv layer applies the 
                bottleneck, and finally 1x1 conv layer increases number of 
                channels by this factor.
        """
        super(ResNet, self).__init__()
        
        if not bottleneck:
            expansion = 1
        else:
            assert expansion > 1, (
                "In bottleneck architecture, expansion must be greater than 1."
            )

        self.Conv1 = nn.Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=3,
            stride=(1, 1),
            padding=1,
            padding_mode=padding_mode,
            bias=False,  # `BatchNorm2d` already has learnable bias
        )
        self.BatchNorm = nn.BatchNorm2d(num_features=out_channels,)
        self.ReLU = nn.ReLU(inplace=True)
        self.globalAvgPool = nn.AdaptiveAvgPool2d(
            output_size=(1, 1)
        )  

        self.blocks = []
        for block in range(len(num_layers_per_block)):
            for j in range(num_layers_per_block[block]):
                if (block != 0 and j == 0):
                    in_channels = 2 ** (block-1) * out_channels * expansion
                elif (block == 0 and j == 0):
                    in_channels = out_channels
                else:
                    in_channels = 2 ** (block) * out_channels * expansion
                block_args = {
                    "in_channels": in_channels,
                    "out_channels": 2 ** (block) * out_channels,  # expansion done internally if bottleneck blocks
                    "stride": stride if (block != 0 and j == 0) else (1, 1),
                    "padding_mode": padding_mode,
                }
                if bottleneck:
                    block_args["expansion"] = expansion
                    self.blocks += [BottleneckBlock(**block_args)]
                else:
                    self.blocks += [ResNetBlock(**block_args)]

        self.model = nn.Sequential(*self.blocks)
        self.linear = nn.Linear(
            in_features=2 ** (len(num_layers_per_block) - 1) * out_channels * \
                expansion,
            out_features=num_classes,
        )

    def forward(self, x):
        """Standard forward pass."""
        x = self.ReLU(self.BatchNorm(self.Conv1(x)))
        x = self.model(x)  # all the ResNet layers
        x = self.globalAvgPool(x)
        # Reshape because of the fully-connected layer:
        x = x.reshape(x.shape[0], -1)
        x = self.linear(x)
        return x


class ResNetBlock(nn.Module):
    """Define a single Resnet block consisting of only two layers."""

    def __init__(
        self,
        in_channels,
        out_channels,
        stride,
        padding_mode,
    ):
        """
        Initialize single ResNet block with only two Conv2d layers. See
        documentation above for parameter explanation.
        """
        super(ResNetBlock, self).__init__()
        self.conv_block = self.build_conv_block(
            in_channels,
            out_channels,
            stride,
            padding_mode,
        )

    def build_conv_block(
        self,
        in_channels,
        out_channels,
        stride,
        padding_mode,
    ):
        """Construct a convolutional block, see documentation above for
        parameter explanation. Returns a conv block (with a conv layer, a
        non-linearity layer (ReLU) and a BatchNorm layer)
        """
        conv_block = []
        conv_block += [
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=3,
                stride=stride,
                padding=1,
                padding_mode=padding_mode,
                bias=False
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=out_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=3,
                stride=(1, 1),
                padding=1,
                padding_mode=padding_mode,
                bias=False
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=out_channels,)]

        self.act_fn = nn.ReLU(inplace=True)

        if stride == (2, 2) or in_channels != out_channels:
            # no ReLU, as done here: 
            # https://github.com/pytorch/vision/blob/main/torchvision/models/resnet.py#L240-L243
            self.downsample = nn.Sequential(
                nn.Conv2d(
                    in_channels=in_channels,
                    out_channels=out_channels,
                    kernel_size=1,
                    stride=2,
                    bias=False
                ),
                nn.BatchNorm2d(num_features=out_channels,)
            )
        else:
            self.downsample = None

        return nn.Sequential(*conv_block)

    def forward(self, x):
        """Forward function (with skip connections)."""
        out = self.conv_block(x)
        if self.downsample is not None:
            residual = self.downsample(x)
        else:
            residual = x
        out += residual  # Add skip connection
        out = self.act_fn(out)
        return out


class BottleneckBlock(nn.Module):
    """
    Define a single Resnet bottleneck block consisting of three layers, cf. 
    Fig. 5 of arXiv:1512.03385.
    """
    def __init__(
        self,
        in_channels,
        out_channels,
        stride,
        padding_mode,
        expansion=4,
    ):
        """
        Initialize single ResNet block with three Conv2d layers.

        Args: 
            --- cf. `ResNet` class ---
            expansion (int): In bottleneck architecture, first 1x1 conv layer
                reduces number of channels by this factor (except in the first 
                layer of each block), then 3x3 conv layer applies the 
                bottleneck, and finally 1x1 conv layer increases number of 
                channels by this factor.
        """
        super().__init__()
        self.conv_block = self.build_conv_block(
            expansion,
            in_channels,
            out_channels,
            stride,
            padding_mode,
        )

    def build_conv_block(
        self,
        expansion,
        in_channels,
        out_channels,
        stride,
        padding_mode,
    ):
        """
        Construct a convolutional block. Returns a conv block (with a conv 
        layer, a non-linearity layer (ReLU) and a BatchNorm layer) x 3.

        Args: 
            expansion: In bottleneck architecture, first 1x1 conv layer
                reduces number of channels by this factor (except in the first 
                layer of each block), then 3x3 conv layer applies the 
                bottleneck, and finally 1x1 conv layer increases number of 
                channels by this factor.
            --- cf. `ResNet` class ---
        """
        conv_block = []
        conv_block += [
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=1,
                stride=1,
                bias=False,
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=out_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=3,
                stride=stride,
                padding=1,
                padding_mode=padding_mode,
                bias=False,
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=out_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels * expansion,
                kernel_size=1,
                stride=1,
                bias=False,
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=out_channels * expansion,)]

        self.act_fn = nn.ReLU(inplace=True)

        if stride == (2, 2) or in_channels != out_channels * expansion:
            # no ReLU, as done here: 
            # https://github.com/pytorch/vision/blob/main/torchvision/models/resnet.py#L240-L243
            self.downsample = nn.Sequential(
                nn.Conv2d(
                    in_channels=in_channels,
                    out_channels=out_channels * expansion,
                    kernel_size=1,
                    stride=2 if stride == (2, 2) else 1,
                    bias=False
                ),
                nn.BatchNorm2d(num_features=out_channels * expansion,)
            )
        else:
            self.downsample = None

        return nn.Sequential(*conv_block)

    def forward(self, x):
        """Forward function (with skip connections)."""
        out = self.conv_block(x)
        if self.downsample is not None:
            residual = self.downsample(x)
        else:
            residual = x
        out += residual  # Add skip connection
        out = self.act_fn(out)
        return out
    