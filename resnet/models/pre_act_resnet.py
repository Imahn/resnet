import torch.nn as nn


class PreActResNet(nn.Module):
    def __init__(
        self,
        num_layers_per_block=[5, 5, 5],
        in_channels=3,
        out_channels=16,
        stride=(2, 2),
        padding_mode="zeros",
        num_classes=10,
        bottleneck=False,
        expansion=1,
    ):
        """Initialize ResNet class.

        Parameters:
            num_layers_per_block (List[int]): Number of layers per block. The 
                number of blocks is given by the length of the list.
            in_channels (int): Number of input channels for the input images.
            out_channels (int): Number of output channels for the first Conv2d
                layer.
            stride ((int, int)): Stride to be used in all Conv2d layers that
                downsample.
            padding_mode (str): Which padding mode to use at the borders of
                the images. Options: "zeros", "reflect", "replicate",
                "circular".
            num_classes (int): Number of classes of the CIFAR10 dataset.
            bottleneck (bool): Whether to use bottleneck architecture or not.
            expansion (int): In bottleneck architecture, first 1x1 conv layer
                reduces number of channels by this factor (except in the first 
                layer of each block), then 3x3 conv layer applies the 
                bottleneck, and finally 1x1 conv layer increases number of 
                channels by this factor.
        """
        super().__init__()

        if not bottleneck:
            expansion = 1
        else:
            assert expansion > 1, (
                "In bottleneck architecture, expansion must be greater than 1."
            )

        # https://github.com/KaimingHe/resnet-1k-layers/blob/master/resnet-pre-act.lua#L114
        self.conv1 = nn.Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=3,
            stride=(1, 1),
            padding=1,
            padding_mode=padding_mode,
            bias=True,
        )
        # from arXiv:1603.05027: "[...] for the last Residual Unit (followed
        # by average pooling and a fullyconnected classifier), we adopt an
        # extra activation right after its element-wise addition."
        # also cf. https://github.com/KaimingHe/resnet-1k-layers/blob/master/resnet-pre-act.lua#L118-L119
        self.batch_norm = nn.BatchNorm2d(
            num_features=2 ** (len(num_layers_per_block) - 1) * out_channels * expansion
        )
        self.act_fn = nn.ReLU(inplace=True)

        self.globalAvgPool = nn.AdaptiveAvgPool2d(
            output_size=(1, 1)
        )

        self.blocks = []
        for block in range(len(num_layers_per_block)):
            for j in range(num_layers_per_block[block]):
                if (block != 0 and j == 0):
                    in_channels = 2 ** (block-1) * out_channels * expansion
                elif (block == 0 and j == 0):
                    in_channels = out_channels
                else:
                    in_channels = 2 ** (block) * out_channels * expansion
                block_args = {
                    "in_channels": in_channels,
                    "out_channels": 2 ** (block) * out_channels,  # expansion done internally if bottleneck blocks
                    "stride": stride if (block != 0 and j == 0) else (1, 1),
                    "padding_mode": padding_mode,
                }
                if bottleneck:
                    block_args["expansion"] = expansion
                    self.blocks += [BottleneckBlock(**block_args)]
                else:
                    self.blocks += [PreActResNetBlock(**block_args)]

        self.model = nn.Sequential(*self.blocks)
        self.linear = nn.Linear(
            in_features=2 ** (len(num_layers_per_block) - 1) * out_channels * \
                expansion,
            out_features=num_classes,
        )

    def forward(self, x):
        """Standard forward pass."""
        x = self.conv1(x)
        x = self.model(x)  # all the ResNet layers
        x = self.batch_norm(x)
        x = self.act_fn(x)
        x = self.globalAvgPool(x)
        # Reshape because of the fully-connected layer:
        x = x.reshape(x.shape[0], -1)
        x = self.linear(x)
        return x


class PreActResNetBlock(nn.Module):
    """
    Define a single pre-activation Resnet block consisting of only two 
    layers as proposed in http://arxiv.org/abs/1603.05027.
    """
    def __init__(
        self,
        in_channels,
        out_channels,
        stride,
        padding_mode,
    ):
        """
        Initialize single ResNet block with only two Conv2d layers. See
        documentation above for parameter explanation.

        Args: cf. `ResNet` class
        """
        super().__init__()
        self.conv_block = self.build_conv_block(
            in_channels,
            out_channels,
            stride,            
            padding_mode,
        )

    def build_conv_block(
        self,
        in_channels,
        out_channels,
        stride,
        padding_mode,
    ):
        """
        Construct a pre-activation convolutional block. Returns a conv block 
        (with a BatchNorm layer, a non-linearity layer (ReLU) and a conv layer) x 2.

        Args: cf. `ResNet` class
        """
        conv_block = []
        conv_block += [nn.BatchNorm2d(num_features=in_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=3,
                stride=stride,
                padding=1,
                padding_mode=padding_mode,
                bias=False,
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=out_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=3,
                stride=(1, 1),
                padding=1,
                padding_mode=padding_mode,
                bias=False,
            )
        ]

        if stride == (2, 2) or in_channels != out_channels:
            # from arXiv:1603.05027: "[...] when preactivation is used, these 
            # projection shortcuts are also with pre-activation."
            self.downsample = nn.Sequential(
                nn.BatchNorm2d(num_features=in_channels,),
                nn.ReLU(inplace=True),
                nn.Conv2d(
                    in_channels=in_channels,
                    out_channels=out_channels,
                    kernel_size=1,
                    stride=2,
                    bias=False,
                ),
            )
        else:
            self.downsample = None

        return nn.Sequential(*conv_block)

    def forward(self, x):
        """Forward function (with skip connections)."""
        out = self.conv_block(x)
        if self.downsample is not None:
            residual = self.downsample(x)
        else:
            residual = x
        out += residual  # Add skip connection
        return out


class BottleneckBlock(nn.Module):
    """
    Define a single pre-activation Resnet bottleneck block consisting of three 
    conv layers.
    """
    def __init__(
        self,
        in_channels,
        out_channels,
        stride,        
        padding_mode,
        expansion=4,
    ):
        """
        Initialize single ResNet block with only two Conv2d layers. See
        documentation above for parameter explanation.

        Args:
            --- cf. `ResNet` class ---
            expansion (int): In bottleneck architecture, first 1x1 conv layer
                reduces number of channels by this factor (except in the first 
                layer of each block), then 3x3 conv layer applies the 
                bottleneck, and finally 1x1 conv layer increases number of 
                channels by this factor.
        """
        super().__init__()
        self.conv_block = self.build_conv_block(
            expansion,
            in_channels,
            out_channels,
            stride,
            padding_mode,
        )

    def build_conv_block(
        self,
        expansion,
        in_channels,
        out_channels,
        stride,
        padding_mode,
    ):
        """
        Construct a pre-activation convolutional block. Returns a conv block 
        (with a BatchNorm layer, a non-linearity layer (ReLU) and a conv layer) x 3.

        Args:
            expansion (int): In bottleneck architecture, first 1x1 conv layer
                reduces number of channels by this factor (except in the first 
                layer of each block), then 3x3 conv layer applies the 
                bottleneck, and finally 1x1 conv layer increases number of 
                channels by this factor.
            --- cf. `ResNet` class ---
        """
        conv_block = []
        conv_block += [nn.BatchNorm2d(num_features=in_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=out_channels,
                kernel_size=1,
                stride=1,
                bias=False,
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=out_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels,
                kernel_size=3,
                stride=stride,
                padding=1,
                padding_mode=padding_mode,
                bias=False,
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=out_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=out_channels,
                out_channels=out_channels * expansion,
                kernel_size=1,
                stride=1,
                bias=False,
            )
        ]

        if stride == (2, 2) or in_channels != out_channels * expansion:
            # from arXiv:1603.05027: "[...] when preactivation is used, these 
            # projection shortcuts are also with pre-activation."
            self.downsample = nn.Sequential(
                nn.BatchNorm2d(num_features=in_channels,),
                nn.ReLU(inplace=True),
                nn.Conv2d(
                    in_channels=in_channels,
                    out_channels=out_channels * expansion,
                    kernel_size=1,
                    stride=2 if stride == (2, 2) else 1,
                    bias=False,
                ),
            )
        else:
            self.downsample = None

        return nn.Sequential(*conv_block)

    def forward(self, x):
        """Forward function (with skip connections)."""
        out = self.conv_block(x)
        if self.downsample is not None:
            residual = self.downsample(x)
        else:
            residual = x
        out += residual  # Add skip connection
        return out
