import torch.nn as nn


class ResNeXt(nn.Module):
    def __init__(
        self,
        num_layers_per_block=[5, 5, 5],
        in_channels=3,
        out_channels=32,
        stride=(2, 2),
        padding_mode="zeros",
        num_classes=10,
        cardinality=32,
        bottleneck_width=1,
        expansion=4,
    ):
        """
        Initialize ResNeXt class, which uses bottleneck blocks only.

        Parameters:
            num_layers_per_block (List[int]): Number of layers per block. The 
                number of blocks is given by the length of the list.
            in_channels (int): Number of input channels for the input images.
            out_channels (int): Number of output channels in the first block.
                After each block, the number of channels is doubled.
            stride ((int, int)): Stride to be used in all Conv2d layers that
                downsample.
            padding_mode (str): Which padding mode to use at the borders of
                the images. Options: "zeros", "reflect", "replicate",
                "circular".
            num_classes (int): Number of classes of the CIFAR10 dataset.
            cardinality (int): Number of groups in the grouped convolution 
                (alternatively number of paths in the aggregated residual 
                transformations).
            bottleneck_width (int): Number of channels in the conv-3x3 layers
                of the bottlenecks are given by
                `cardinality * bottleneck_width`.
            expansion (int): Number of output channels of last conv-1x1 layer 
                is given by expansion * input channels of first conv-1x1 
                layer in structure 
                ```
                [1x1, in_channels] -> [3x3, bottleneck_width * cardinality] -> 
                [1x1, expansion * in_channels]
                ```
                cf. Table 1 of https://arxiv.org/abs/1611.05431.
        """
        super().__init__()

        self.Conv1 = nn.Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=3,
            stride=(1, 1),
            padding=1,
            padding_mode=padding_mode,
            bias=False,  # `BatchNorm2d` already has learnable bias
        )
        self.BatchNorm = nn.BatchNorm2d(num_features=out_channels)
        self.ReLU = nn.ReLU(inplace=True)
        self.globalAvgPool = nn.AdaptiveAvgPool2d(output_size=(1, 1))

        self.blocks = []
        for block in range(len(num_layers_per_block)):
            for j in range(num_layers_per_block[block]):
                if (block != 0 and j == 0):
                    in_channels = 2 ** (block-1) * out_channels * expansion
                elif (block == 0 and j == 0):
                    in_channels = out_channels
                else:
                    in_channels = 2 ** (block) * out_channels * expansion
                self.blocks += [
                    BottleneckBlock(
                        in_channels=in_channels,
                        middle_channels=2 ** block * cardinality * \
                            bottleneck_width,
                        out_channels=2 ** block * out_channels,  # expansion done internally if bottleneck blocks
                        stride=stride if (block != 0 and j == 0) else (1, 1),
                        padding_mode=padding_mode,
                        expansion=expansion,
                        cardinality=cardinality,
                    )
                ]

        self.model = nn.Sequential(*self.blocks)
        self.linear = nn.Linear(
            in_features=2 ** (len(num_layers_per_block) - 1) * out_channels * \
                expansion,
            out_features=num_classes,
        )

    def forward(self, x):
        """Standard forward pass."""
        x = self.ReLU(self.BatchNorm(self.Conv1(x)))
        x = self.model(x)  # all the ResNet layers
        x = self.globalAvgPool(x)
        # Reshape because of the fully-connected layer:
        x = x.reshape(x.shape[0], -1)
        x = self.linear(x)
        return x


class BottleneckBlock(nn.Module):
    """
    Define a single ResNeXt bottleneck block consisting of three layers, cf. 
    Fig. 3c) of arXiv:1611.05431.
    """
    def __init__(
        self,
        in_channels,
        out_channels,
        stride,
        padding_mode,
        middle_channels,
        expansion=4,
        cardinality=32,
    ):
        """
        Initialize single ResNet block with three Conv2d layers.

        Args: 
            --- cf. `ResNet` class ---
            middle_channels (int): Number of input and output channels in the 
                conv-3x3 layer
            expansion (int): Number of output channels of last conv-1x1 layer 
                is given by expansion * input channels of first conv-1x1 
                layer in structure 
                ```
                [1x1, in_channels] -> [3x3, bottleneck_width * cardinality] -> 
                [1x1, expansion * in_channels]
                ```
                cf. Table 1 of https://arxiv.org/abs/1611.05431.
            cardinality (int): Number of groups in the grouped convolution
                (alternatively number of paths in the aggregated residual
                transformations).
        """
        super().__init__()
        self.conv_block = self.build_conv_block(
            expansion,
            cardinality,
            in_channels,
            middle_channels,
            out_channels,
            stride,
            padding_mode,
        )

    def build_conv_block(
        self,
        expansion,
        cardinality,
        in_channels,
        middle_channels,
        out_channels,
        stride,
        padding_mode,
    ):
        """
        Construct a convolutional block. Returns a conv block (with a conv 
        layer, a non-linearity layer (ReLU) and a BatchNorm layer) x 3. The
        middle conv layer uses a grouped convolution.

        Args:
            expansion (int): Number of output channels of last conv-1x1 layer 
                is given by expansion * input channels of first conv-1x1 
                layer in structure 
                ```
                [1x1, in_channels] -> [3x3, bottleneck_width * cardinality] -> 
                [1x1, expansion * in_channels]
                ```
                cf. Table 1 of https://arxiv.org/abs/1611.05431.
            cardinality (int): Number of groups in the grouped convolution
                (alternatively number of paths in the aggregated residual
                transformations).
            middle_channels (int): Number of input and output channels in the
                conv-3x3 layer
            --- cf. `ResNet` class ---
        """
        conv_block = []
        conv_block += [
            nn.Conv2d(
                in_channels=in_channels,
                out_channels=middle_channels,
                kernel_size=1,
                stride=1,
                bias=False,
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=middle_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=middle_channels,
                out_channels=middle_channels,
                kernel_size=3,
                stride=stride,
                padding=1,
                padding_mode=padding_mode,
                groups=cardinality,
                bias=False,
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=middle_channels,)]
        conv_block += [nn.ReLU(inplace=True)]
        conv_block += [
            nn.Conv2d(
                in_channels=middle_channels,
                out_channels=out_channels * expansion,
                kernel_size=1,
                stride=1,
                bias=False,
            )
        ]
        conv_block += [nn.BatchNorm2d(num_features=out_channels * expansion)]

        self.act_fn = nn.ReLU(inplace=True)

        if stride == (2, 2) or in_channels != out_channels * expansion:
            # no ReLU, as done here: 
            # https://github.com/pytorch/vision/blob/main/torchvision/models/resnet.py#L240-L243
            self.downsample = nn.Sequential(
                nn.Conv2d(
                    in_channels=in_channels,
                    out_channels=out_channels * expansion,
                    kernel_size=1,
                    stride=2 if stride == (2, 2) else 1,
                    bias=False,
                ),
                nn.BatchNorm2d(num_features=out_channels * expansion)
            )
        else:
            self.downsample = None

        return nn.Sequential(*conv_block)

    def forward(self, x):
        """Forward function (with skip connections)."""
        out = self.conv_block(x)
        if self.downsample is not None:
            residual = self.downsample(x)
        else:
            residual = x
        out += residual  # Add skip connection
        out = self.act_fn(out)
        return out
    