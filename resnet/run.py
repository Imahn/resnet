import os
import time
from datetime import datetime
from warnings import warn

import numpy as np
import torch
from matplotlib import pyplot as plt
from matplotlib import ticker
from torch import autocast, nn, optim
from torch.utils.data import DataLoader, random_split
from torchinfo import summary
from torchvision import datasets, transforms
from zeus.monitor import ZeusMonitor

from lr_schedulers import *
from models.resnet import ResNet
from models.pre_act_resnet import PreActResNet
from models.wide_resnet import WideResNet
from train_options import TrainOptions
from models.resnext import ResNeXt


def save_checkpoint(state, filename="my_checkpoint.pth.tar"):
    """Creates a model checkpoint to save and load a model. The ending
    <.pth.tar> is commonly used for this.

    Params:
        state (dictionary): The state of the model and optimizer in a dictionary.
        filename (pth.tar): The name of the checkpoint.
    """
    torch.save(state, filename)
    print("--> Saving checkpoint.")


def load_checkpoint(model, optimizer, checkpoint):
    """Load an existing checkpoint of the model to continue training.

    Params:
        model (torch.nn): Model that should be trained further.
        optimizer (torch.optim): Optimizer that was used.
        checkpoint (torch.load): Checkpoint for continuing to train.
    """
    model.load_state_dict(state_dict=checkpoint["state_dict"])
    optimizer.load_state_dict(state_dict=checkpoint["optimizer"])
    print("--> The checkpoint of the model is being loaded.")


@torch.no_grad()
def check_accuracy(loader, model, use_amp):
    """ "Check the accuracy.

    Params:
        loader (torch.utils.data.DataLoader): DataLoader of the dataset that
            we want to test our model on.
        model (torch.nn.Module): Model that we are training.
        use_amp (bool): Whether to use automatic mixed precision.
    """
    model.eval()
    num_correct = 0
    num_samples = 0

    for images, labels in loader:
        images = images.to(device=device)
        images = torch.squeeze(
            input=images, dim=1
        )  # shape: (batch_size, 28, 28), otherwise RNN throws error
        labels = labels.to(device=device)

        with autocast(
            device_type=labels.device.type,
            dtype=torch.float16,
            enabled=use_amp,
        ):
            forward_pass = model(images)  # shape: (batch_size, 10)
        _, predictions = forward_pass.max(
            dim=1
        )  # from our model, we get the shape (batch_size, 10) returned
        num_correct += (predictions == labels).sum()
        num_samples += predictions.size(0)

    print(
        f"Got {num_correct}/{num_samples} with accuracy "
        f"{float(num_correct)/float(num_samples) * 100:.2f} /"
    )


def produce_loss_plot(num_epochs, train_losses, val_losses, saving_path):
    """Plot the categorical crossentropy (loss) evolving over time.

    Params:
        num_epochs (int): Number of epochs the model was trained.
        train_losses (numpy.array): Training losses per epoch.
        val_losses (numpy.array): Validation losses per epoch.
        saving_path (str): Saving path for the loss plot.
    """
    epochs = np.arange(start=0, stop=num_epochs, step=1)
    fig, ax = plt.subplots()
    plt.plot(epochs, train_losses, label="Training")
    plt.plot(epochs, val_losses, label="Validation")
    if num_epochs < 10:
        loc = ticker.MultipleLocator(base=1.0)
    else:
        loc = ticker.MultipleLocator(base=num_epochs // 10)
    ax.xaxis.set_major_locator(loc)
    plt.xlabel("Epoch")
    plt.ylabel("Loss (Categorical Crossentropy)")
    plt.legend()
    plt.savefig(
        os.path.join(
            saving_path,
            "loss_" + datetime.now().strftime("%d-%m-%Y-%H:%M") + ".pdf",
        ),
        bbox_inches="tight",
        pad_inches=0.01,
    )
    plt.close()


def produce_acc_plot(
    num_epochs, train_accuracies, val_accuracies, saving_path
):
    """Plot the accuracy evolving over time.

    Params:
        num_epochs (int): Number of epochs the model was trained.
        train_accuracies (numpy.array): Training accuracies per epoch.
        val_accuracies (numpy.array): Validation accuracies per epoch.
        saving_path (str): Saving path for the loss plot.
    """
    epochs = np.arange(start=0, stop=num_epochs, step=1)
    fig, ax = plt.subplots()
    plt.plot(epochs, train_accuracies, label="Training")
    plt.plot(epochs, val_accuracies, label="Validation")
    if num_epochs < 10:
        loc = ticker.MultipleLocator(base=1.0)
    else:
        loc = ticker.MultipleLocator(base=num_epochs // 10)
    ax.xaxis.set_major_locator(loc)
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.savefig(
        os.path.join(
            saving_path,
            "accuracy_" + datetime.now().strftime("%d-%m-%Y-%H:%M") + ".pdf",
        ),
        bbox_inches="tight",
        pad_inches=0.01,
    )
    plt.close()


@torch.no_grad()
def produce_and_print_confusion_matrix(
    num_classes, test_loader, use_amp, model, saving_path
):
    """Produce a confusion matrix based on the test set [1].

    [1] https://stackoverflow.com/questions/33828780/matplotlib-display-array-values-with-imshow

    Params:
        num_classes (int)                           -- Number of classes NN 
            has to predict at the end.
        test_loader (torch.utils.data.DataLoader)   -- DataLoader for the test 
            dataset.
        use_amp (bool)                              -- Whether to use automatic
            mixed precision.
        model (torch.nn)                            -- Model that was trained.
        saving_path (str)                           -- Saving path for the  
            loss plot.
    """
    model.eval()

    confusion_matrix = torch.zeros(num_classes, num_classes)
    counter = 0
    
    for i, (inputs, classes) in enumerate(test_loader):
        inputs = inputs.to(device)
        inputs = torch.squeeze(
            input=inputs, dim=1
        )  # shape: (batch_size, 28, 28), otherwise RNN throws error
        classes = classes.to(device)
        with autocast(
            device_type=classes.device.type,
            dtype=torch.float16,
            enabled=use_amp,
        ):
            outputs = model(inputs)
        _, preds = torch.max(outputs, 1)
        for t, p in zip(classes.reshape(-1), preds.reshape(-1)):
            confusion_matrix[t, p] += 1
            counter += 1

    # Because of the random split in the datasets, the classes are imbalanced.
    # Thus, we should do a normalization across each label in the confusion
    # matrix:
    for i in range(num_classes):
        total_sums = 0
        for element in confusion_matrix[i]:
            total_sums += element
        confusion_matrix[i] /= total_sums

    confusion_matrix = confusion_matrix.detach().cpu().numpy()
    name_classes = np.array(
        [
            "Airplane",
            "Automobile",
            "Bird",
            "Cat",
            "Deer",
            "Dog",
            "Frog",
            "Horse",
            "Ship",
            "Truck",
        ]
    )

    fig, ax = plt.subplots()
    font = {"size": 7}
    for (i, j), label in np.ndenumerate(confusion_matrix):
        ax.text(j, i, round(label, 3), ha="center", va="center", fontdict=font)
        ax.text(j, i, round(label, 3), ha="center", va="center", fontdict=font)
    plt.imshow(confusion_matrix, cmap="jet")
    plt.colorbar(ticks=np.arange(start=0, stop=1.0, step=0.15))
    tick_marks = np.arange(start=0, stop=num_classes)
    plt.xticks(ticks=tick_marks, labels=name_classes, rotation=60)
    plt.yticks(ticks=tick_marks, labels=name_classes)
    plt.xlabel("Predicted Label")
    plt.ylabel("True Label")
    plt.savefig(
        os.path.join(
            saving_path,
            "confusion_matrix_"
            + datetime.now().strftime("%d-%m-%Y-%H:%M")
            + ".pdf",
        ),
        bbox_inches="tight",
        pad_inches=0.01,
    )
    plt.close()
    print("Confusion matrix: \n{}".format(confusion_matrix))


if __name__ == "__main__":
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    args = TrainOptions().args
    if args.load_cp:
        args.num_epochs = 0
    if args.use_bottleneck_arch:
        assert args.expansion is not None, (
            "If the bottleneck architecture is used, the expansion factor "
            "must be specified."
        )
    else:
        if args.model_type != "wide-resnet" and args.model_type != "resnext":
            args.expansion = None
    if args.use_nesterov and (args.optimizer != "SGD" or args.momentum == 0):
            raise ValueError(
                "Nesterov momentum is supposed to be used. Make sure to a) "
                "use SGD optimizer via `--optim SGD` and b) to use non-zero "
                "momentum, e.g. `--momentum 0.9`."
            )
    print(
        "Total number of GPUs available: {}, GPUs: {}".format(
            torch.cuda.device_count(),
            [
                torch.cuda.get_device_name(i)
                for i in range(torch.cuda.device_count())
            ],
        )
    )
    print(args)

    # Transformations for the train and validation datasets:
    if args.use_augmentation:
        if args.use_normalization:
            trafo_train_val = transforms.Compose(
                [
                    transforms.Pad(
                        padding=4,
                        fill=0,
                        padding_mode=args.padding_mode,
                    ),
                    transforms.RandomHorizontalFlip(p=0.5),
                    transforms.RandomCrop(size=(32, 32)),
                    transforms.ToTensor(),
                    transforms.Normalize(
                        mean=(0.4914, 0.4822, 0.4465),
                        std=(0.247, 0.2435, 0.2616),
                    ),
                ]
            )
        else:
            trafo_train_val = transforms.Compose(
                [
                    transforms.Pad(
                        padding=4,
                        fill=0,
                        padding_mode=args.padding_mode,
                    ),
                    transforms.RandomHorizontalFlip(p=0.5),
                    transforms.RandomCrop(size=(32, 32)),
                    transforms.ToTensor(),
                ]
            )
    else:
        trafo_train_val = transforms.ToTensor()
    # Transformation for the test dataset (use no augmentation on the test dataset, just as the ResNet paper didn't):
    if args.use_normalization:
        trafo_test = transforms.Compose(
            [
                transforms.ToTensor(),
                transforms.Normalize(
                    mean=(0.4914, 0.4822, 0.4465), std=(0.247, 0.2435, 0.2616)
                ),
            ]
        )
    else:
        trafo_test = transforms.ToTensor()

    # Dataset loading:
    cifar10_full = datasets.CIFAR10(
        root=args.root,
        train=True,
        transform=trafo_train_val,
        target_transform=None,
        download=False,
    )
    train_subset, val_subset = random_split(
        dataset=cifar10_full,
        lengths=[
            int(args.train_split * cifar10_full.__len__()),
            int(args.val_split * cifar10_full.__len__()),
        ],
    )
    train_loader = DataLoader(
        dataset=train_subset,
        shuffle=True,
        batch_size=args.batch_size,
        num_workers=args.num_workers,
        pin_memory=args.pin_memory,
    )
    val_loader = DataLoader(
        dataset=val_subset,
        shuffle=True,
        batch_size=args.batch_size,
        num_workers=args.num_workers,
        pin_memory=args.pin_memory,
    )
    cifar10_test_dataset = datasets.CIFAR10(
        root=args.root,
        train=False,
        transform=trafo_test,
        target_transform=None,
        download=False,
    )
    test_loader = DataLoader(
        dataset=cifar10_test_dataset,
        shuffle=True,
        batch_size=args.batch_size,
        num_workers=args.num_workers,
        pin_memory=args.pin_memory,
    )
    print(
        "We have in total {} train, {} validation and {} test samples".format(
            train_subset.__len__(),
            val_subset.__len__(),
            cifar10_test_dataset.__len__(),
        )
    )
    print(
        "Shape of one sample in the CIFAR-10 dataset: {}".format(
            train_subset[0][0].shape
        )
    )  # (3, 32, 32)

    # Build residual network:
    model_args = {
        "num_layers_per_block": args.num_layers_per_block,
        "in_channels": args.in_channels,
        "out_channels": args.out_channels,
        "stride": args.stride,
        "num_classes": args.num_classes,
    }
    if args.model_type == "pre-act-resnet" or args.model_type == "resnet":
        model_args |= {
            "bottleneck": args.use_bottleneck_arch,
            "expansion": args.expansion,
        }
        if args.dropout_rate > 0:
            warn(
                "Dropout rate is set to a value greater than 0. "
                "This is only supported for the 'wide-resnet' model. "
                "Dropout will not be used."
            )
    elif args.model_type == "wide-resnet":
        if args.use_bottleneck_arch:
            warn(
                "Bottleneck architecture is not supported for the "
                "'wide-resnet' model. Bottleneck architecture will not be used"
            )
        model_args |= {
            "dropout_rate": args.dropout_rate,
            "expansion": args.expansion,
        }
    elif args.model_type == "resnext":
        model_args |= {
            "cardinality": args.cardinality,
            "bottleneck_width": args.bottleneck_width,
            "expansion": args.expansion,
        }

    if args.model_type == "pre-act-resnet":
        model = PreActResNet(**model_args)
    elif args.model_type == "resnet":
        model = ResNet(**model_args)
    elif args.model_type == "wide-resnet":
        model = WideResNet(**model_args)
    elif args.model_type == "resnext":
        model = ResNeXt(**model_args)
    else:
        raise NotImplementedError(
            "Only supported model types in this implementation are "
            "'resnet', 'pre-act-resnet', 'wide-resnet' and 'resnext'."
        )

    # Use torch.nn.DataParallel() to utilize more than one GPU:
    if torch.cuda.device_count() > 1:
        model = nn.DataParallel(model)
    model.to(device)
    print(summary(model, input_size=(args.batch_size, 3, 32, 32), verbose=1))

    # Loss and optimizer:
    lossFunction = nn.CrossEntropyLoss()
    if args.optimizer == "SGD":
        optimizer = optim.SGD(
            params=model.parameters(),
            lr=args.learning_rate,
            momentum=args.momentum,
            nesterov=args.use_nesterov,
            weight_decay=args.weight_decay,
        )
    elif args.optimizer == "Adam":
        optimizer = optim.AdamW(
            params=model.parameters(),
            lr=args.learning_rate,
            betas=(0.9, 0.999),
            eps=1e-08,
            weight_decay=args.weight_decay,
        )
    else:
        raise NotImplementedError(
            "The only supported optimizers in this implementation are SGD and "
            "Adam."
        )

    if args.load_cp:
        load_checkpoint(
            model=model,
            optimizer=optimizer,
            checkpoint=torch.load(f=args.loading_path),
        )
    
    # Learning rate schedulers
    # Use one for warm-up, as described in original ResNet paper
    scheduler1 = ConstantLR(
        optimizer=optimizer, factor=1/10, total_iters=5, verbose=True
    )
    scheduler2 = optim.lr_scheduler.MultiStepLR(
        optimizer=optimizer, milestones=args.milestones, gamma=args.lr_drop_factor, verbose=True
    )
    chained_scheduler = ChainedScheduler([scheduler1, scheduler2])
    model.train()

    # Train model:
    start_time = time.time()
    train_losses = np.array([])
    val_losses = np.array([])
    train_accuracies = np.array([])
    val_accuracies = np.array([])
    # For saving the model with the highest accuracy on the validation dataset:
    max_val_acc = 1 / args.num_classes

    if device.type == "cuda":
        monitor = ZeusMonitor(
            gpu_indices=[i for i in range(torch.cuda.device_count())]
        )
        monitor.begin_window("training")

    for epoch in range(args.num_epochs):
        # Store losses to print for each epoch
        trainingLoss_perEpoch = np.array([])  # training loss per epoch
        valLoss_perEpoch = np.array([])  # validation loss per epoch
        # Initialize epoch time:
        t0 = datetime.now()

        # Variables for calculating the accuracy:
        num_correct = 0
        num_samples = 0
        val_num_correct = 0  # for the validation accuracy
        val_num_samples = 0  # for the validation accuracy

        for batch_idx, (images, labels) in enumerate(train_loader):
            # Reshape data & get to cuda if possible
            images = images.to(device)  # shape: (batch_size, 1, 28, 28)
            labels = labels.to(device)
            batch_size = images.shape[0]
            with autocast(
                device_type=labels.device.type,
                dtype=torch.float16,
                enabled=args.use_amp,
            ):
                # Train model (forward pass):
                output = model(images)
                loss = lossFunction(output, labels)

            # Calculate accuracy (don't waste computation by calculating
            # gradients when we don't need them):
            with torch.no_grad():
                model.eval()
                output_maxima, max_indices = torch.max(
                    output, dim=1, keepdim=False
                )
                # from our model, we get predictions of the shape
                # [batch_size, C], where C is the num of classes and in the
                # case of MNIST, C = 10
                num_correct += torch.sum(input=(max_indices == labels))
                num_samples += batch_size
                assert batch_size == max_indices.size(0), (
                    "The first index of output of the forward pass and the "
                    "batch size do not agree!"
                )
                # max_indices.size(0) is already an int
            model.train()
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            # Append training loss to array for averaging:
            trainingLoss_perEpoch = np.append(
                trainingLoss_perEpoch, loss.detach().cpu()
            )

            # Code for printouts concerning progress of training:
            if batch_idx % 1 == 0:
                print(
                    "Train Epoch: %d [ %05d / %d (%f %%)]\tTraining Loss: %f \tElapsed Time: %f s"
                    % (
                        epoch,
                        batch_idx * len(images),
                        len(train_loader.dataset),
                        100.0
                        * batch_idx
                        * len(images)
                        / len(train_loader.dataset),
                        np.round_(loss.detach().cpu(), decimals=4),
                        np.round_(
                            (datetime.now() - t0).total_seconds(), decimals=2
                        ),
                    )
                )
        # Validation stuff (don't waste computation by calculating gradients
        # when we don't need them):
        with torch.no_grad():
            model.eval()
            for val_batch_idx, (val_images, val_labels) in enumerate(
                val_loader
            ):
                # print(val_images.shape)
                val_images = val_images.to(device)
                val_images = torch.squeeze(
                    input=val_images, dim=1
                )  # shape: (batch_size, 28, 28), otherwise RNN throws error
                val_labels = val_labels.to(device)
                batch_size = val_images.shape[0]
                with autocast(
                    device_type=val_labels.device.type,
                    dtype=torch.float16,
                    enabled=args.use_amp,
                ):
                    # Forward pass on the val dataset (no backprop!):
                    val_output = model(val_images)
                    val_loss = lossFunction(val_output, val_labels)

                # Calculate accuracy:
                val_output_maxima, val_max_indices = torch.max(
                    val_output, dim=1, keepdim=False
                )
                # from our model, we get predictions of the shape
                # [batch_size, C], where C is the num of classes and in the case of MNIST, C = 10
                val_num_correct += torch.sum(
                    input=(val_max_indices == val_labels)
                )
                val_num_samples += batch_size
                assert batch_size == val_max_indices.size(0), (
                    "The first index of output of the forward pass and the "
                    "batch size do not agree!"
                )
                # val_max_indices.size(0) is already an int

                # Append val loss to array for averaging:
                valLoss_perEpoch = np.append(
                    valLoss_perEpoch, val_loss.detach().cpu()
                )

                # Code for printouts concerning progress of validating:
                if val_batch_idx % 20 == 0 and val_batch_idx != 0:
                    print(
                        "Valid Epoch: %d [ %05d / %d (%f %%)]\tValidation Loss: %f \tElapsed Time: %f s"
                        % (
                            epoch,
                            val_batch_idx * len(val_images),
                            len(val_loader.dataset),
                            100.0
                            * val_batch_idx
                            * len(val_images)
                            / len(val_loader.dataset),
                            np.round_(val_loss.detach().cpu(), decimals=4),
                            np.round_(
                                (datetime.now() - t0).total_seconds(),
                                decimals=2,
                            ),
                        )
                    )

        # Calculate accuracies for each epoch:
        train_acc = int(num_correct) / num_samples
        train_accuracies = np.append(train_accuracies, train_acc)
        # For the validation dataset:
        val_acc = int(val_num_correct) / val_num_samples
        val_accuracies = np.append(val_accuracies, val_acc)
        # Append training losses to corresponding arrays for plotting:
        train_losses = np.append(
            train_losses, np.average(trainingLoss_perEpoch)
        )
        val_losses = np.append(val_losses, np.average(valLoss_perEpoch))

        print(
            "Epoch {:02}: {:.2f} sec ...".format(
                epoch, (datetime.now() - t0).total_seconds(), end="\n\n"
            )
        )
        print(
            "Averaged training loss: ",
            round(np.average(trainingLoss_perEpoch), 4),
            "\tTraining accuracy: {:.2f}".format(train_acc * 100),
            "%",
            end="\n",
        )
        print(
            "Averaged validation loss: ",
            round(np.average(valLoss_perEpoch), 4),
            "\tValidation accuracy: {:.2f}".format(val_acc * 100),
            "%",
            end="\n\n\n",
        )
        if args.save_best_model:
            if val_acc > max_val_acc:
                checkpoint = {
                    "state_dict": model.state_dict(),
                    "optimizer": optimizer.state_dict(),
                }
                save_checkpoint(
                    state=checkpoint,
                    filename=os.path.join(
                        args.saving_path, "max_val_acc.pth.tar"
                    ),
                )
                print(
                    "The validation accuracy takes its maximum in epoch {}".format(
                        epoch
                    )
                )
                max_val_acc = val_acc

        model.train()
        chained_scheduler.step()

    if args.load_cp:
        print("Checking accuracy on test data:")
        check_accuracy(test_loader, model, args.use_amp)
    else:
        if device.type == "cuda":
            measurement = monitor.end_window("training")
        print(
            f"The whole training of {args.num_epochs} epoch(s) took "
            f"{round(time.time() - start_time, 2)} seconds."
            f"\nEnergy consumption of entire training = "
            f"{measurement.total_energy / 1e3:.3f} [kJ]"
        )
        print("Checking accuracy on training data:")
        check_accuracy(train_loader, model, args.use_amp)

    if args.load_cp:
        produce_and_print_confusion_matrix(
            args.num_classes, test_loader, args.use_amp, model,
            args.saving_path
        )
    else:
        produce_loss_plot(
            args.num_epochs, train_losses, val_losses, args.saving_path
        )
        produce_acc_plot(
            args.num_epochs, train_accuracies, val_accuracies, args.saving_path
        )
