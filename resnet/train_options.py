import argparse


class TrainOptions:
    """This class includes train options."""

    def __init__(self):
        parser = argparse.ArgumentParser("Parameters and hyperparameters.")
        parser.add_argument(
            "--model_type",
            required=False,
            type=str,
            default="resnet",
            help=(
                "Type of resnet to train. Options: 'resnet' (original ResNet),"
                " 'pre-act-resnet' (pre-activation ResNet), 'wide-resnet' "
                "(WideResNet) and 'resnext' (ResNeXt)."
            ),
        )
        parser.add_argument(
            "--dropout_rate",
            required=False,
            type=float,
            default=0.0,
            help="Dropout rate if `--model_type 'wide-resnet'` is chosen.",
        )
        parser.add_argument(
            "--use_amp",
            action="store_true",
            help="Whether to use automatic mixed precision (AMP).",
        )
        parser.add_argument(
            "--use_bottleneck_arch",
            action="store_true",
            help="Whether to use the bottleneck architecture.",
        )
        parser.add_argument(
            "--expansion",
            type=int,
            default=4,
            help=(
                "Expansion factor for the bottleneck architecture if "
                "`--model_type` is `pre-act-resnet` or `resnet`, for "
                "`wide-resnet` this is the width."
            ),
        )
        parser.add_argument(
            "--bottleneck_width",
            type=int,
            default=4,
            help=(
                "Width of the bottleneck architecture if `--model_type` is "
                "`resnext`."
            ),
        )
        parser.add_argument(
            "--cardinality",
            type=int,
            default=32,
            help=(
                "Number of groups in the grouped convolution (alternatively "
                "number of paths in the aggregated residual transformations)."
            ),
        )
        parser.add_argument(
            "--saving_path",
            required=False,
            type=str,
            default="/beegfs/desy/user/shekhzai/ResidualNetwork/Trainings/imahn-version/",
            help="Saving path for the files (loss plot, accuracy plot, etc.)",
        )
        parser.add_argument(
            "--root",
            required=False,
            type=str,
            default="../Datasets/CIFAR10/",
            help="Location of the dataset.",
        )
        # Hyperparameters for training:
        parser.add_argument(
            "--learning_rate",
            required=False,
            type=float,
            default=1e-1,
            help="Learning rate for the training of the NN.",
        )
        parser.add_argument(
            "--num_epochs",
            required=False,
            type=int,
            default=90,
            help="Number of epochs used for training of the NN.",
        )
        parser.add_argument(
            "--batch_size",
            required=False,
            type=int,
            default=64,
            help="Number of batches that are used for one  update rule.",
        )
        parser.add_argument(
            "--weight_decay",
            required=False,
            type=float,
            default=1e-6,
            help="Weight decay to use with the ADAM optimizer.",
        )
        parser.add_argument(
            "--use_augmentation",
            action="store_true",
            default=False,
            help="Whether to use data use data augmentation on the training and validation dataset or not.",
        )
        parser.add_argument(
            "--use_normalization",
            action="store_true",
            default=False,
            help="Whether to normalize the training, validation AND test dataset.",
        )
        parser.add_argument(
            "--train_split",
            required=False,
            type=float,
            default=9e-1,
            help="Which portion of the CIFAR-10 training dataset to use for training.",
        )
        parser.add_argument(
            "--val_split",
            required=False,
            type=float,
            default=1e-1,
            help="Which portion of the CIFAR-10 training dataset to use for validation.",
        )
        parser.add_argument(
            "--padding_mode",
            required=False,
            type=str,
            default="constant",
            help="Which padding mode to use at the borders of the images.",
        )
        parser.add_argument(
            "--lr_drop_factor",
            required=False,
            type=float,
            default=0.1,
            help=(
                "Factor by which to drop the learning rate at specific "
                "milestones passed via `--milestones`."
            ),
        )
        parser.add_argument(
            "--milestones",
            required=False,
            type=int,
            nargs="*",
            default=[80, 120],
        )
        parser.add_argument(
            "--optimizer",
            required=False,
            type=str,
            default="SGD",
            help="The optimizer that is used for training. Options: SGD | Adam.",
        )
        parser.add_argument(
            "--momentum",
            required=False,
            type=float,
            default=9e-1,
            help="Momentum factor for the SGD optimizer.",
        )
        parser.add_argument(
            "--use_nesterov",
            action="store_true",
            default=False,
            help="Whether to use Nesterov momentum for the SGD optimizer.",
        )
        parser.add_argument(
            "--save_best_model",
            action="store_true",
            default=False,
            help="Whether to save one checkpoint for the best model with respect to the validation accuracy.",
        )
        parser.add_argument(
            "--num_workers",
            type=int,
            default=0,
            help="Number of subprocesses to use in the training and validation DataLoader. Zero workers mean that the data are loaded in the main process.",
        )
        parser.add_argument(
            "--pin_memory",
            action="store_true",
            help="Whether to copy tensors into CUDA pinned memory before they are returned. This generally speeds up the data loading process.",
        )
        # Hyperparameters for the ResNet model:
        parser.add_argument(
            "--num_layers_per_block",
            required=False,
            type=int,
            nargs="*",
            default=[5, 5, 5],
            help=(
                "Number of layers per block. Number of blocks is given by the "
                "length of the list.",
            )
        )
        parser.add_argument(
            "--in_channels",
            required=False,
            type=int,
            default=3,
            help="Number of input channels for the input images.",
        )
        parser.add_argument(
            "--out_channels",
            required=False,
            type=int,
            default=16,
            help="Number of output channels for the first Conv2d layer.",
        )
        parser.add_argument(
            "--stride",
            required=False,
            type=int,
            nargs=2,
            default=(2, 2),
            help="Stride to be used in all Conv2d layers that downsample.",
        )
        parser.add_argument(
            "--num_classes",
            required=False,
            type=int,
            default=10,
            help="Number of classes of the CIFAR10 dataset.",
        )
        
        # Parameters for continuing to train:
        parser.add_argument(
            "--load_cp",
            action="store_true",
            default=False,
            help="Whether to load preexisting checkpoint(s) of the model.",
        )
        parser.add_argument(
            "--loading_path",
            required=False,
            type=str,
            default="/beegfs/desy/user/shekhzai/ResNet/Trainings/normalized/dropout/max_val_acc.pth.tar",
            help="Loading path for the checkpoint. Only makes sense in combination with <loadCp>.",
        )
        self.args = parser.parse_args()
