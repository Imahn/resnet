#!/usr/bin/env python

from distutils.core import setup

from setuptools import find_packages

setup(
    description='ResNet implementation in PyTorch for CIFAR10',
    author='Imahn Shekhzadeh',
    author_email='imahn.shekhzadeh@hesge.ch',
    packages=find_packages(),
)