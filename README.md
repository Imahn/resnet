# ResNe(X)t

Residual networks, pre-activation residual networks, wide residual networks and ResNeXt, as proposed in [arXiv:1512.03385](https://arxiv.org/pdf/1512.03385), [arXiv:1603.05027](http://arxiv.org/abs/1603.05027), [arXiv:1605.07146](http://arxiv.org/abs/1605.07146) and [arXiv:1611.05431](http://arxiv.org/abs/1611.05431) respectively. The original residual networks are also called "post-activation ResNet" in the second paper.

## Description
The above mentioned papers apply residual networks (ResNet), pre-activation residual networks and wide residual networks to the ImageNet and [CIFAR10](https://www.cs.toronto.edu/~kriz/cifar.html) datasets. This repository contains a PyTorch implementation of the ResNet and pre-activation resnet applied on CIFAR10. 


Calculation of number of layers/depth (assuming that same number of layers $n$ is used per block):
- For a bottleneck architecture (regardless of whether the model is a pre- or post-activation ResNet), the total number of layers (depth) is $1 + \text{num\_blocks} \cdot 3 \cdot n + 1$. Assuming $3$ blocks, this yields $9n+2$ layers. 

- For a non-bottleneck (basic) architecture, the number of layers is $1 + \text{num\_blocks} \cdot 2 \cdot n + 1$; for $3$ blocks, this yields $6n + 2$. 

- For the WideResNets, the authors consider **all** convolutional layers, and hence the depths is given by $1 + \text{num\_blocks} \cdot (2 \cdot n + 1)$. Assuming $3$ blocks, this yields a depth of $6n + 4$.

- For the ResNeXt, the architecture is a bottleneck, and hence the number of layers is $9n + 2$, assuming that there are $3$ blocks in total.


## Visuals
For a wide ResNet with depth 28 and width 10 (see further down), this is the confusion matrix I obtained: 

<img src="Images/WRN-28-10-conf-matrix.png" width="600" alt="confusion matrix">

## Usage
### Original (Post-Activation) ResNet
For a ResNet with 32 layers, I obtain a test error of $`8.17\%`$ (i.e. $`1-\text{test}\_\text{accuracy}`$)
using the following options:  
```python
python -B resnet/run.py --batch_size 128 --num_layers_per_block 5 5 5 --num_epochs 200 --save_best_model --saving_path '/beegfs/desy/user/shekhzai/ResNet/Trainings/resnet_32/' --use_augmentation --use_normalization --weight_decay 1e-4
```
This is $0.66 \%$ worse than reported in the original paper.

To run a ResNet with in total 110 layers, change the following arguments of the above command: 
```python
--num_layers_per_block 18 18 18 --saving_path '/beegfs/desy/user/shekhzai/ResNet/Trainings/resnet_110/'
```
Concretely, the test error I obtain is $`7.13\%`$, the paper reports a result of $`(6.61 \pm 0.16)\%`$ among 5 runs (note that I only ran the script once). 

For a ResNet with in total 1202 layers,
```python
--num_layers_per_block 200 200 200 --saving_path '/beegfs/desy/user/shekhzai/ResNet/Trainings/resnet_1202/'
```
I get a test error of $7.94 \%$, the paper reports $7.93 \%$. The number of parameters is about $19.5$ millions, only slightly bigger
than the $19.4$ millions reported in the original paper.

**Note that the small deviations in test error (in particular for ResNet-32 and ResNet-110) could be because of a higher batch size I chose.
The original paper chose a batch size of 64 per GPU, I use a batch size of 128 per GPU.**

To see all available argparsing arguments, check out the file `resnet/train_options.py`. 

### Pre-Activation ResNet
Add the flag `--model_type "pre-act-resnet"`. For a 110-layer pre-activation ResNet (`--num_layers_per_block 18 18 18`), I obtain a test error of $6.87 \%$, whereas [the pre-activation ResNet paper](http://arxiv.org/abs/1603.05027) reports a median (among five runs) of $6.37 \%$. For the 1202-layer pre-activation ResNet (`--num_layers_per_block 200 200 200`), I get a test error of $6.40 \%$.

### Bottleneck
To use a bottlneck architecture, add the flags
```python
--use_bottleneck_arch --expansion 4 
```
A 164-layer bottleneck post-activation ResNet (`--num_layers_per_block 18 18 18`) brings down the test error to $5.89 \%$, which is on par with a reported median (among five runs) of $5.93 \%$ in [arXiv:1603.05027](http://arxiv.org/abs/1603.05027). The number of parameters is with about $1.7$ M almost the same as a non-bottleneck 110-layer post-activation ResNet. The pre-activation version achieves a test error of $5.86 \%$, which is slightly higher than the reported median (among five runs) of $5.46 \%$.

For a 1001-layer bottleneck post-activation ResNet with about 10.3 M params, add the flags
```python
--num_layers_per_block 111 111 111 --use_bottleneck_arch --expansion 4 
```
which increases the test error to $7.87 \%$ (reported median test error among five runs in [arXiv:1603.05027](http://arxiv.org/abs/1603.05027) is $7.61 \%$). The pre-activation version reaches a test error of $5.46 \%$, which is slightly worse than the reported $4.92 \%$.

### Wide ResNet
The wide ResNet in [arXiv:1605.07146](http://arxiv.org/abs/1605.07146) only uses basic ResNet building blocks, no bottlenecks. Note that the `expansion` argument in this context is also called "width" in the paper and denoted by $k$.

To run a wide ResNet, specifically the best-performing model on Cifar10 (denoted by WK-28-10 in [this paper](http://arxiv.org/abs/1605.07146) with about 36.5M parameters), run 
```python
python -B resnet/run.py --batch_size 128 --num_layers_per_block 4 4 4 --expansion 10 --num_epochs 200 --save_best_model --saving_path '/beegfs/desy/user/shekhzai/ResNet/Trainings/resnet_32/' --padding_mode "reflect" --dropout_rate 0.3 --use_augmentation --use_normalization --lr_drop_factor 0.2 --milestones 60 120 160 --use_nesterov --weight_decay 5e-4 --model_type "wide-resnet"
```
In one run, I obtain a test error of $3.91 \%$, which is on par with a reported median (among five runs) of $3.89 \%$ in the literature.

For a WK-40-10 (`--num_layers_per_block 6 6 6`), I obtain a slightly worse test error of $4.12 \%$.

### ResNeXt
ResNeXt only uses bottleneck blocks.

To run a ResNeXt-29 (16x64d) with about 68.2M params,
```python
python -B resnet/run.py --batch_size 128 --num_layers_per_block 3 3 3 --num_epochs 300 --save_best_model --saving_path . --use_augmentation --use_normalization --weight_decay 5e-4 --model_type "resnext" --out_channels 64 --expansion 4 --cardinality 16 --bottleneck_width 64 --milestones 150 225
```
The test error I obtain is $4.32 \%$, whereas the [ResNeXt paper](http://arxiv.org/abs/1611.05431) reports a mean (among ten runs) of $3.58 \%$.

## Support
If you find any bug, please file an issue or a pull request! 


## Project status
This project will not be developed further. 