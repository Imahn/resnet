import logging

import torch

from resnet import ResNet


def test_forward_pass():
    """
    Test the forward pass of the ResNet model.
    """
    x = torch.zeros((64, 3, 32, 32))
    model = ResNet()
    assert model(x).shape == torch.Size([64, 10])
    logging.info(
        f"ResNet model: {model}\n\n Shape of forward pass: "
        f"{model(x).shape}"
    )


test_forward_pass()
